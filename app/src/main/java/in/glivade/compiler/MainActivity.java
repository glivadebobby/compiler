package in.glivade.compiler;

import static com.nononsenseapps.filepicker.Utils.getFileForUri;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import in.glivade.compiler.app.ToastBuilder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int REQUEST_FILE = 4;
    private Context mContext;
    private LinearLayout mLayoutResponse;
    private FloatingActionButton mButtonAdd;
    private Button mButtonSubmit;
    private EditText mEditTextProgram;
    private TextView mTextViewResponse;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_FILE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            try {
                mEditTextProgram.setText(getStringFromFile(getFileForUri(uri).getAbsolutePath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonAdd) {
            pickFile();
        } else if (v == mButtonSubmit) {
            displayFileNameDialog();
        }
    }

    private void initObjects() {
        mLayoutResponse = (LinearLayout) findViewById(R.id.layout_response);
        mButtonAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);
        mEditTextProgram = (EditText) findViewById(R.id.input_program);
        mTextViewResponse = (TextView) findViewById(R.id.txt_response);

        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonAdd.setOnClickListener(this);
        mButtonSubmit.setOnClickListener(this);
    }

    private void pickFile() {
        Intent intent = new Intent(mContext, JavaFilePickerActivity.class);
        intent.putExtra(JavaFilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        intent.putExtra(JavaFilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        intent.putExtra(JavaFilePickerActivity.EXTRA_MODE, JavaFilePickerActivity.MODE_FILE);
        intent.putExtra(JavaFilePickerActivity.EXTRA_START_PATH,
                Environment.getExternalStorageDirectory().getPath());
        startActivityForResult(intent, REQUEST_FILE);
    }

    private void uploadFile(String filePath) {
        Ion.with(this).load("POST", "http://compiler.glivade.in")
                .addMultipartParts(new FilePart("program", new File(filePath)))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        hideProgressDialog();
                        if (e == null) {
                            int status = result.getHeaders().code();
                            if (status == 200) {
                                mLayoutResponse.setVisibility(View.VISIBLE);
                                mTextViewResponse.setText(result.getResult());
                                mTextViewResponse.requestFocus();
                            } else {
                                ToastBuilder.build(mContext, getString(R.string.error_unexpected));
                            }
                        } else {
                            ToastBuilder.build(mContext, e.getMessage());
                        }
                    }
                });
    }

    private String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    private String getStringFromFile(String filePath) throws IOException {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        fin.close();
        return ret;
    }

    @SuppressLint("InflateParams")
    private void displayFileNameDialog() {
        View fileNameView = LayoutInflater.from(mContext).inflate(R.layout.dialog_file_name, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(fileNameView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        final EditText editTextFileName = (EditText) fileNameView.findViewById(
                R.id.input_file_name);
        Button buttonSubmit = (Button) fileNameView.findViewById(R.id.btn_submit);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName = editTextFileName.getText().toString().trim();
                if (TextUtils.isEmpty(fileName)) {
                    editTextFileName.setError("File name cannot be empty");
                } else {
                    alertDialog.dismiss();
                    showProgressDialog("Compiling..");
                    uploadFile(saveFile(fileName));
                }
            }
        });
    }

    private String saveFile(String fileName) {
        File fileDirectory = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name));

        boolean isDirectory = true;
        if (!fileDirectory.exists()) {
            isDirectory = fileDirectory.mkdirs();
        }

        if (isDirectory) {
            String filePath = fileDirectory.getPath() + File.separator + fileName + ".java";

            try {
                FileOutputStream fileOutputStream = new FileOutputStream(filePath);
                fileOutputStream.write(mEditTextProgram.getText().toString().getBytes());
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return filePath;
        }
        return null;
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
